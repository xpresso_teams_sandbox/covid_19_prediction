"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""

import os
import sys
import shutil
import helper
import inference
import config
import pandas as pd

import logging
from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

logger = XprLogger("inference_service_run_timeterror",level=logging.INFO)


class InferenceServiceRunTimeterror(AbstractInferenceService):
    """ Main class for the inference service. User will need to implement
    following functions:
       - load_model: It gets the directory of the model stored as parameters,
           user will need to implement the method for loading the model and
           updating self.model variable
       - transform_input: It gets the JSON object from the rest API as input.
           User will need to implement the method to convert the json data to
           relevant feature vector to be used for prediction
       - predict: It gets the feature vector or any other object from the
           transform_input method. User will need to implement the model
           prediction codebase here. It returns the predicted value
       - transform_output: It gets the predicted value from the predict method.
           User need to implement this method to converted predicted method
           to JSON serializable object. Response from this method is send back
           to the client as JSON Object.

    AbstractInferenceService automatically creates the flask reset api
    with resource /predict for this class.
    Request JSON format:
       {
         "input" : <input_goes_here> // It could be any JSON object
       }
       This value of "input" key is sent to transform_input

    Response JSON format:
       {
         "message": "success/failure",
         "results": <response goes here> // It could be any JSOn object
       }
       Output of transform_output goes as value of "results"
    """

    def __init__(self, src_file):
        super().__init__()
        """ Initialize any static data required during boot up """
        self.test_file_path = src_file
        self.mount_path = "/data/"

    def load_model(self, model_path):
        """ This is used to load the model on the boot time.
        User will need to load the model and save it in the variable
        self.model. It is IMPORTANT to update self.model.
        Args:
            model_path(str): Path of the directory where the model files are
               stored.
        """
        dest = config.MASTER_PATH
        if not os.path.exists(dest):
            os.makedirs(dest)
        shutil.copytree(model_path, dest)
        self.src_file = os.path.join(self.mount_path, self.test_file_path)
        print("source file printed !! ! ",self.src_file);
        test_data_1 = pd.read_excel(self.src_file)
        print("loaded succesfully 655 ",test_data_1); 
        helper.create_folders()
        dst_file = config.MASTER_PATH + 'data/input/test_data.xlsx'
        shutil.copy(self.src_file, dst_file)
        shutil.copy(os.path.join(self.mount_path, 'country_metadata.csv'), config.MASTER_PATH + 'data/input/country_metadata.csv')

        ''' INFERENCE PIPELINE '''
        inference.main()

    def user_def_func(self):
        """ This is used to load the model on the boot time.
        User will need to load the model and save it in the variable
        self.model. It is IMPORTANT to update self.model.
        Args:
            model_path(str): Path of the directory where the model files are
               stored.
        """
        dest = config.MASTER_PATH
        if not os.path.exists(dest):
           os.makedirs(dest)
        shutil.copytree(self.OUTPUT_DIR, dest)
        self.src_file = os.path.join(self.mount_path, self.test_file_path)
        print("source file printed !! ! ",self.src_file);
        test_data_1 = pd.read_excel(self.src_file)
        print("loaded succesfully 655 ",test_data_1); 
        helper.create_folders()
        dst_file = config.MASTER_PATH + 'data/input/test_data.xlsx'
        shutil.copy(self.src_file, dst_file)
        shutil.copy(os.path.join(self.mount_path, 'country_metadata.csv'), config.MASTER_PATH + 'data/input/country_metadata.csv')

        ''' INFERENCE PIPELINE '''
        inference.main()

    def transform_input(self, input_request):
        """
        Convert the raw input request to a feature data or any other object
        to be used during prediction

        Args:
            input_request: JSON Object or an array received from the REST API,
               which needs to be converted into relevant feature data.

        Returns:
            obj: Any transformed object
        """
        pass

    def predict(self, input_request):
        """
        This method implements the prediction method of the supported model.
        It gets the output of transform_input as input and returns the predicted
        value

        Args:
            input_request: Transformed object outputted from transform_input
               method

        Returns:
            obj: predicted value from the model

        """
        pass

    def transform_output(self, output_response):
        """
        Convert the predicted value into a relevant JSON serializable object.
        This is sent back to the REST API call
        Args:
            output_response: Predicted output from predict method.

        Returns:
          obj: JSON Serializable object
        """
        pass


if __name__ == "__main__":
    pred = InferenceServiceRunTimeterror(src_file=sys.argv[2])
    # === To run locally. Use load_model instead of load. ===
    # pred.load_model(model_path="/model_path/") instead of pred.load()
    # pred.load()
    user_def_func()
    pred.run_api(port=5000)
