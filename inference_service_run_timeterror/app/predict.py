''' insert comments '''
import warnings
from datetime import datetime, timedelta
import pandas as pd
import numpy as np
import helper
import modeling
import config
warnings.filterwarnings("ignore")

def prophet_test(country_name, days=15):
    ''' insert comments '''
    filename = filename = config.MASTER_PATH + 'data/model/prophet/' + country_name + '.pkl'
    model = helper.load_model(filename)
    future = model.make_future_dataframe(periods=days)
    prediction = model.predict(future)
    prediction['Confirmed'] = prediction['yhat'].map(np.exp).map(int)
    prediction = prediction.rename({'ds': 'Date'}, axis=1)
    return prediction[['Date', 'Confirmed']].tail(days)

def lin_reg_test(country_name, days=15):
    ''' insert comments '''
    filename = config.MASTER_PATH + 'data/model/lin_regr/' + country_name + '.pkl'
    model_param = helper.load_model(filename)
    start = model_param['records'] + 1
    intercept = model_param['intercept']
    slope = model_param['slope']
    conf = [round(np.exp(slope * item + intercept)) for item in range(start, start + days)]
    start_date = model_param['start_date']
    var = '%Y-%m-%d'
    start_date = datetime.strptime(start_date, var)
    dates = [(start_date + timedelta(days=item)).strftime(var) for item in range(1, days + 1)]
    prediction = pd.DataFrame({'Date': dates, 'Confirmed': conf})
    return prediction

def predicting_model(countries):
    ''' insert comments '''
    for country in countries:
        country_name = country.replace(' ', '_')
        prophet_pred = prophet_test(country_name)
        prophet_pred['Country'] = country
        prophet_pred.to_csv(config.MASTER_PATH + 'data/output/prophet/' + country_name + '.csv', index=False)
        lin_reg_pred = lin_reg_test(country_name)
        lin_reg_pred['Country'] = country
        lin_reg_pred.to_csv(config.MASTER_PATH + 'data/output/lin_regr/' + country_name + '.csv', index=False)

def predicting_invalid(countries, days=15):
    ''' insert comments '''
    global_slope = modeling.get_global_model()
    for country in countries:
        country_name = country.replace(' ', '_')
        country_data = pd.read_csv(config.MASTER_PATH + 'data/intermediate/' + country_name + '.csv')
        week_avg = int(np.mean(list(country_data.tail(7)['Daily'])))
        country_data = country_data.tail(1)
        last_day = list(country_data['Daily'])[0]
        start_val = max(last_day, week_avg)
        start_date = list(country_data['Date'])[0]
        var = '%Y-%m-%d'
        start_date = datetime.strptime(start_date, var)
        dates = [(start_date + timedelta(days=item)).strftime(var) for item in range(1, days + 1)]
        conf = [round(np.exp(global_slope * item + np.log(start_val))) for item in range(days)]
        prediction = pd.DataFrame({'Date': dates, 'Confirmed': conf})
        prediction['Country'] = country
        prediction.to_csv(config.MASTER_PATH + 'data/output/prophet/' + country_name + '.csv', index=False)
        prediction.to_csv(config.MASTER_PATH + 'data/output/lin_regr/' + country_name + '.csv', index=False)
