''' insert comments '''
import warnings
import pandas as pd
import numpy as np
import config
warnings.filterwarnings("ignore")

def chart_data_helper(master_data, world_data, country_metadata):
    ''' insert comments '''
    country_latest_data = master_data.sort_values('Date', ascending=False).groupby('Country').first().reset_index()
    req_cols = ['Country', 'Confirmed', 'Daily']
    country_latest_data = country_latest_data.sort_values('Confirmed', ascending=False)[req_cols]
    country_latest_data = pd.merge(country_latest_data, country_metadata[['Country', 'Code']])
    world_latest_data = world_data.sort_values('Date', ascending=False).head(1)
    world_latest_data = world_latest_data.drop('Date', axis=1)
    world_latest_data = pd.merge(world_latest_data, country_metadata[['Country', 'Code']])
    return country_latest_data, world_latest_data

def prepare_ui_data():
    ''' insert comments '''
    country_metadata = pd.read_csv(config.MASTER_PATH + 'data/input/country_metadata.csv')
    world_data = pd.read_csv(config.MASTER_PATH + 'data/intermediate/world_data.csv')
    master_data = pd.read_csv(config.MASTER_PATH + 'data/intermediate/master_data.csv')
    country_latest_data, world_latest_data = chart_data_helper(master_data, world_data, country_metadata)
    world_latest_data.to_csv(config.MASTER_PATH + 'data/ui/current/world_latest_data.csv', index=False)
    country_latest_data.to_csv(config.MASTER_PATH + 'data/ui/current/country_latest_data.csv', index=False)

def prepare_chart_data(data, country_metadata):
    ''' insert comments '''
    data_temp = data.tail(8)
    data_temp = pd.merge(data_temp, country_metadata)
    scale = 10*1000*1000
    data_temp['Per_ten_million'] = data_temp['Confirmed'] // (data_temp['Population'] / scale)
    mult_factor = list(data_temp.head(1)['Per_ten_million'])[0]
    data_temp['Mult_factor'] = data_temp['Per_ten_million'] / mult_factor
    last_day = list(data_temp.tail(1)['Daily'])[0]
    prev_week = sum(data_temp.head(7)['Daily'])
    data_temp['Last_day'] = last_day
    data_temp['Prev_week'] = prev_week
    return data_temp

def get_chart_data():
    ''' insert comments '''
    country_metadata = pd.read_csv(config.MASTER_PATH + 'data/input/country_metadata.csv')
    world_data = pd.read_csv(config.MASTER_PATH + 'data/intermediate/world_data.csv')
    world_chart_data = prepare_chart_data(world_data, country_metadata)
    world_chart_data.to_csv(config.MASTER_PATH + 'data/ui/current/world.csv', index=False)
    world_chart_data = world_chart_data.tail(1)
    world_last_day = list(world_chart_data['Last_day'])[0]
    world_prev_week = list(world_chart_data['Prev_week'])[0]
    world_fact = list(world_chart_data['Mult_factor'])[0]
    train_countries = list(country_metadata['Country'])
    train_countries.remove('World')
    for country in train_countries:
        country_name = country.replace(' ', '_')
        temp_data = pd.read_csv(config.MASTER_PATH + 'data/intermediate/' + country_name + '.csv')
        country_chart_data = prepare_chart_data(temp_data, country_metadata)
        country_name = country.replace(' ', '_')
        country_chart_data.to_csv(config.MASTER_PATH + 'data/ui/current/' + country_name + '.csv', index=False)
        short_data = get_chart_short_data(country_chart_data, world_fact)
        short_data['ld_world'] = world_last_day
        short_data['pw_world'] = world_prev_week
        short_data.to_csv(config.MASTER_PATH + 'data/ui/current_short/' + country_name + '.csv', index=False)

def get_chart_short_data(country_temp_data, world_fact):
    temp_data = country_temp_data.tail(1)
    mult_fact = list(temp_data['Mult_factor'])[0]
    severity_col, severity_val = 'green', 'Low'
    if mult_fact > world_fact:
        severity_col, severity_val = 'red', 'High'
    temp_data['Severity_col'] = severity_col
    temp_data['Severity_val'] = severity_val
    return temp_data

def get_chart_data_pred(country_metadata, temp_data, start_val, scale):
    temp_data['Confirmed'] = temp_data['Confirmed'].map(int)
    temp_data = temp_data.rename({'Confirmed' : 'Daily'}, axis=1)
    temp_data['Confirmed'] = (temp_data['Daily'].cumsum() + start_val).map(int)
    temp_data = pd.merge(temp_data, country_metadata)
    temp_data['Per_ten_million'] = (temp_data['Confirmed'] // (temp_data['Population'] / scale)).map(int)
    temp_data['Mult_factor'] = np.round(temp_data['Confirmed'] / start_val, 2)
    return temp_data

def get_world_pred(country_metadata, master_data, scale, days=15):
    world_latest_data = pd.read_csv(config.MASTER_PATH + 'data/ui/current/country_latest_data.csv')
    start_val = list(world_latest_data['Confirmed'])[0]
    world_data = master_data.groupby('Date', as_index=False).agg({'Confirmed': sum}).tail(days)
    world_data['Country'] = 'World'
    world_data = get_chart_data_pred(country_metadata, world_data, start_val, scale)
    world_data.to_csv(config.MASTER_PATH + 'data/ui/predicted/World.csv', index=False)
    return world_data

def prepare_pred_data():
    ''' insert comments '''
    master_data = pd.read_csv(config.MASTER_PATH + 'data/output/predictions.csv')
    country_latest_data = pd.read_csv(config.MASTER_PATH + 'data/ui/current/country_latest_data.csv')
    country_metadata = pd.read_csv(config.MASTER_PATH + 'data/input/country_metadata.csv')
    scale = 10*1000*1000
    world_data = get_world_pred(country_metadata, master_data, scale)
    countries = list(master_data.Country.unique())
    all_data = []
    for country in countries:
        country_name = country.replace(' ', '_')
        start_val = list(country_latest_data[country_latest_data['Country'] == country]['Confirmed'])[0]
        temp_data = master_data[master_data['Country'] == country]
        temp_data = get_chart_data_pred(country_metadata, temp_data, start_val, scale)
        temp_data.to_csv(config.MASTER_PATH + 'data/ui/predicted/' + country_name + '.csv', index=False)    
        all_data.append(temp_data)
    master_data = pd.concat(all_data)
    country_latest_data, world_latest_data = chart_data_helper(master_data, world_data, country_metadata)
    world_latest_data.to_csv(config.MASTER_PATH + 'data/ui/predicted/world_latest_data.csv', index=False)
    country_latest_data.to_csv(config.MASTER_PATH + 'data/ui/predicted/country_latest_data.csv', index=False)
