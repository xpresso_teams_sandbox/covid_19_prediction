''' insert comments '''
import os
import pickle
import warnings
import pandas as pd
import numpy as np
import config
warnings.filterwarnings("ignore")

def create_folders():
    ''' insert comments '''
    folders = ['data', 'data/input', 'data/intermediate', 'data/model', 'data/output', \
        'data/model/prophet', 'data/model/lin_regr',  'data/output/prophet',  'data/output/lin_regr', \
        'data/ui/current', 'data/ui/current_short', 'data/ui/predicted']
    for folder_name in folders:
        path_name = config.MASTER_PATH + folder_name
        if not os.path.exists(path_name):
            os.makedirs(path_name)

def load_training_data():
    ''' insert comments '''
    train_path, train_sheet = config.MASTER_PATH + 'data/input/covid19_train.xlsx', 'in'
    train_data = pd.read_excel(train_path, sheet_name=train_sheet)
    return train_data

def load_test_data():
    ''' insert comments '''
    test_path, test_sheet = config.MASTER_PATH + 'data/input/test_data.xlsx', 'Sheet1'
    test_data = pd.read_excel(test_path, sheet_name=test_sheet)
    test_countries = list(test_data['Country'].unique())
    return test_countries

def feature_transform(country_name):
    ''' insert comments '''
    country_train_data = pd.read_csv(config.MASTER_PATH + 'data/intermediate/' + country_name + '.csv')
    threshold = list(country_train_data.tail(1)['Confirmed'])[0] // 100
    country_train_data = country_train_data[country_train_data['Confirmed'] > threshold]
    country_train_data['Daily_log'] = country_train_data['Daily'].map(np.log)
    country_train_data = country_train_data[abs(country_train_data['Daily_log']) != np.inf]
    return country_train_data

def save_model(data, filename):
    ''' insert comments '''
    output_dict = open(filename, 'wb')
    pickle.dump(data, output_dict)
    output_dict.close()
    return True

def load_model(filename):
    ''' insert comments '''
    input_dict = open(filename, 'rb')
    data = pickle.load(input_dict)
    input_dict.close()
    return data

def aggregate_pred(input_folder):
    ''' insert comments '''
    files = os.listdir(input_folder)
    data_all = [pd.read_csv(os.path.join(input_folder, item)) for item in files]
    data_all = pd.concat(data_all)
    data_all.to_csv(config.MASTER_PATH + 'data/output/predictions.csv', index=False)
    data_all.to_csv(config.MASTER_PATH + 'prediction.csv', index=False)
