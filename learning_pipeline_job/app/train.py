''' insert comments '''
import warnings
import pandas as pd
import data_prep
import helper
import modeling
import ui_data
import config
warnings.filterwarnings("ignore")


def main():
    #Fetching train data and external data
    TRAIN_DATA = helper.load_training_data()

    #Preprocessing data
    data_prep.process_data(TRAIN_DATA)

    #Create data for UI
    ui_data.get_chart_data()
    ui_data.prepare_ui_data()

    #Fetch train countries
    COUNTRY_LATEST_DATA = pd.read_csv(config.MASTER_PATH + 'data/ui/current/country_latest_data.csv')
    COUNTRY_VALID = list(COUNTRY_LATEST_DATA[COUNTRY_LATEST_DATA['Confirmed'] >= 100]['Country'])

    #Training model
    modeling.generating_model(COUNTRY_VALID)
