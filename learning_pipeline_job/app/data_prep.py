''' insert comments '''
import warnings
import pandas as pd
import config
warnings.filterwarnings("ignore")

def get_daily_data(train_data, train_countries):
    ''' insert comments '''
    master_data = []
    for country in train_countries:
        country_data = train_data[train_data['Country'] == country]
        country_data['Confirmed_1'] = country_data['Confirmed'].shift(1)
        country_data['Confirmed_1'] = country_data['Confirmed_1'].fillna(0).map(int)
        country_data['Daily'] = country_data['Confirmed'] - country_data['Confirmed_1']
        country_data['Daily'] = country_data['Daily'].clip(lower=0)
        country_data = country_data[['Country', 'Date', 'Confirmed', 'Daily']]
        country_name = country.replace(' ', '_')
        country_data.to_csv(config.MASTER_PATH + 'data/intermediate/' + country_name + '.csv', index=False)
        master_data.append(country_data)
    master_data = pd.concat(master_data)
    return master_data

def process_data(train_data):
    ''' insert comments '''
    exclude_carriers = ['Diamond Princess', 'MS Zaandam']
    train_data = train_data.drop(['Province', 'Lat', 'Long'], axis=1)
    train_data = train_data[~train_data['Country'].isin(exclude_carriers)]
    train_data = train_data.groupby(['Country', 'Date'], as_index=False).agg({'Confirmed': sum})
    train_data = train_data.sort_values(['Date', 'Country'])
    train_countries = list(train_data['Country'].unique())
    master_data = get_daily_data(train_data, train_countries)
    master_data.to_csv(config.MASTER_PATH + 'data/intermediate/master_data.csv', index=False)
    world_data = master_data.groupby('Date', as_index=False).agg({'Confirmed': sum, 'Daily': sum})
    world_data['Country'] = 'World'
    world_data.to_csv(config.MASTER_PATH + 'data/intermediate/world_data.csv', index=False)
