import pandas as pd
import numpy as np
import os
from bottle import route, run, static_file
import config

@route('/<filename:re:.*\.js>')
def stylesheets(filename):
    return static_file(filename, root='js')

@route('/<filename:re:.*\.css>')
def stylesheets(filename):
    return static_file(filename, root='css')

@route('/<filename:re:.*\.(jpg|png|gif|ico|svg)>')
def images(filename):
    return static_file(filename, root='img')

def get_world_overall_curr():
    data = pd.read_csv(config.MASTER_PATH + 'data/ui/current/world_latest_data.csv')
    data_packet = (list(data['Confirmed'])[0], list(data['Daily'])[0])
    return data_packet

def get_country_overall_curr():
    data = pd.read_csv(config.MASTER_PATH + 'data/ui/current/country_latest_data.csv')
    data['Code'] = data['Code'].fillna('NA')
    str_all = ''
    for _, item in data.iterrows():
        name, code = item['Country'], item['Code'].lower()
        daily, total = str(item['Daily']), str(item['Confirmed'])
        strval = '''<tr><td><object type="image/svg+xml" data="../'''+code+'''.svg" height="16px" 
        width="16px" align="center"></td><td id='td1'>
        <a style='text-decoration:none' href='/current/'''+name+''''>'''+name+'''</td><td align="right" 
        width="70px">'''+total+'''</td><td align="right" width="60px">'''+daily+'''</td></tr>'''
        str_all += strval
    return str_all

def get_country_short_curr(country):
    country_name = country.replace(' ', '_')
    data = pd.read_csv(config.MASTER_PATH + 'data/ui/current_short/' + country_name + '.csv')
    name = list(data['Country'])[0]
    continent = list(data['Continent'])[0]
    pop = round(list(data['Population'])[0] / (1*1000*1000), 2)
    popden = list(data['Density'])[0]
    age = list(data['Age'])[0]
    aff = list(data['Confirmed'])[0]
    sevcol = list(data['Severity_col'])[0]
    sevval = list(data['Severity_val'])[0]
    ld_country = list(data['Last_day'])[0]
    pw_country = list(data['Prev_week'])[0]
    ld_world = list(data['ld_world'])[0]
    pw_world = list(data['pw_world'])[0]
    data = (name, continent, pop, popden, age, aff, sevcol, sevval, ld_country, pw_country, ld_world, pw_world)
    return data

def get_plots_data_curr(country):
    country_name = country.replace(' ', '_')
    data = pd.read_csv(config.MASTER_PATH + 'data/ui/current/' + country_name + '.csv')
    country_daily = ','.join(data['Daily'].map(str))
    country_total = ','.join(data['Per_ten_million'].map(str))
    country_fact = ','.join(data['Mult_factor'].map(str))
    data = pd.read_csv(config.MASTER_PATH + 'data/ui/current/world.csv')
    dates = "'" + "','".join(data['Date']) + "'"
    world_daily = "'" + "','".join(data['Daily'].map(str)) + "'"
    world_total = "'" + "','".join(data['Per_ten_million'].map(str)) + "'"
    world_fact = "'" + "','".join(np.round(data['Mult_factor'], 2).map(str)) + "'"
    data_packet = (dates, country_daily, country_total, country_fact, world_daily, world_total, world_fact)
    return data_packet

def get_world_overall_pred():
    data_pred = pd.read_csv(config.MASTER_PATH + 'data/ui/predicted/world_latest_data.csv')
    data_curr = pd.read_csv(config.MASTER_PATH + 'data/ui/current/world_latest_data.csv')
    data_packet = (list(data_curr['Confirmed'])[0], list(data_pred['Confirmed'])[-1])
    return data_packet

def get_country_overall_pred():
    data_curr = pd.read_csv(config.MASTER_PATH + 'data/ui/current/country_latest_data.csv')
    data_pred = pd.read_csv(config.MASTER_PATH + 'data/ui/predicted/country_latest_data.csv')[['Country', 'Confirmed']]
    data_pred = data_pred.rename({'Confirmed': 'Predicted'}, axis=1)
    data = pd.merge(data_curr, data_pred)
    data['Code'] = data['Code'].fillna('NA')
    str_all = ''
    for _, item in data.iterrows():
        name, code = item['Country'], item['Code'].lower()
        pred, total = str(item['Predicted']), str(item['Confirmed'])
        strval = '''<tr><td><object type="image/svg+xml" data="../'''+code+'''.svg" height="16px" 
        width="16px" align="center"></td><td id='td1'>
        <a style='text-decoration:none' href='/predict/'''+name+''''>'''+name+'''</td><td align="right" 
        width="70px">'''+total+'''</td><td align="right" width="100px">'''+pred+'''</td></tr>'''
        str_all += strval
    return str_all

def get_plots_data_pred(country):
    country_name = country.replace(' ', '_')
    curr = pd.read_csv(config.MASTER_PATH + 'data/ui/current/' + country_name + '.csv')
    code = list(curr['Code'])[-1].lower()
    curr = list(curr['Confirmed'])[-1]
    data = pd.read_csv(config.MASTER_PATH + 'data/ui/predicted/' + country_name + '.csv')
    pred = list(data['Confirmed'])[-1]
    country_daily = ','.join(data['Daily'].map(str))
    country_total = ','.join(data['Per_ten_million'].map(str))
    country_fact = ','.join(data['Mult_factor'].map(str))
    data = pd.read_csv(config.MASTER_PATH + 'data/ui/predicted/World.csv')
    dates = "'" + "','".join(data['Date']) + "'"
    world_daily = ",".join(data['Daily'].map(str))
    world_total = ",".join(data['Per_ten_million'].map(str))
    world_fact = ",".join(np.round(data['Mult_factor'], 2).map(str))
    tab3 = (code, curr, pred)
    data_packet = (dates, country_daily, country_total, country_fact, world_daily, world_total, world_fact, tab3)
    return data_packet

def get_country_overall_over():
    data = pd.read_csv(config.MASTER_PATH + 'data/ui/current/country_latest_data.csv')
    data['Code'] = data['Code'].fillna('NA')
    total_countries = len(data)
    data = data.head(5)
    top_country = list(data['Country'])[0]
    top_aff = list(data['Confirmed'])[0]
    str_all = ''
    for _, item in data.iterrows():
        name, code = item['Country'], item['Code'].lower()
        daily, total = str(item['Daily']), str(item['Confirmed'])
        strval = '''<tr><td><object type="image/svg+xml" data="../'''+code+'''.svg" height="16px" 
        width="16px" align="center"></td><td id='td1'>
        <a style='text-decoration:none' href='/current/'''+name+''''>'''+name+'''</td><td align="right" 
        width="70px">'''+total+'''</td><td align="right" width="60px">'''+daily+'''</td></tr>'''
        str_all += strval
    return str_all, total_countries, top_country, top_aff

def get_plots_data_over():
    data = pd.read_csv(config.MASTER_PATH + 'data/ui/current/world.csv')
    dates = "'" + "','".join(data['Date']) + "'"
    world_daily = "'" + "','".join(data['Daily'].map(str)) + "'"
    world_total = "'" + "','".join(data['Per_ten_million'].map(str)) + "'"
    world_fact = "'" + "','".join(np.round(data['Mult_factor'], 2).map(str)) + "'"
    data_packet = (dates, world_daily, world_total, world_fact)
    return data_packet

def get_country_short_over():
    data = pd.read_csv(config.MASTER_PATH + 'data/ui/current_short/India.csv')
    ld_world = list(data['ld_world'])[0]
    pw_world = list(data['pw_world'])[0]
    return ld_world, pw_world

@route('/current/<country>')
def main(country):
    world_overall = get_world_overall_curr()
    country_overall = get_country_overall_curr()
    country_short = get_country_short_curr(country)
    plot_data = get_plots_data_curr(country)
    dates = plot_data[0]

    msg_1 = '''<!DOCTYPE html>
    <html lang="en">
    <head>
    <title></title>
    <meta charset="utf-8" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.10/c3.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.6/d3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.10/c3.min.js"></script>
    </head>
    '''
    style = '''<style>
    a {text-decoration:none; font-color:white; color:white;}
    #chart1 {height: 200px;width: 500px;}
    #chart2 {height: 200px;width: 500px;}
    #chart3 {height: 200px;width: 200px;}
    #chart4 {height: 200px;width: 200px;}
    #chart5 {height: 200px;width: 400px;}
    table {border-collapse: collapse; color:#eba134; font-family: "Segoe UI Light"}
    #table1 {color:white;text-align:right}
	#table2 {color:white;}
	#table3 {color:black;overflow:scroll;}
	#td1 {text-align:left; width:150px}
	#td1:hover {background-color:#eba134; font-color:black; text-color:black; color:black;}
	#td2:hover {background-color:#eba134; font-color:black; text-color:black; color:black;}
	td, tr {border: 1px solid white;padding:1px 3px 1px 3px;}
	path.domain { stroke: white; }
	.tick text { fill: white; }
	.c3-legend-item text { fill: white; }
	#div1::-webkit-scrollbar {display: none;}
	#div1 {-ms-overflow-style: none;}
	#div1 {position:absolute; left:15px; top:82px; height:510px; overflow:auto;}
	</style>'''
   
    tab3 = '''<body bgcolor="black">

	<table style='margin-left:9px'>
	<tr style="color:white;text-align:center">
	<td id='td2'><a style='text-decoration:none' href='/overview'>Overview</td>
	<td bgcolor=#eba134><font color="black">Current</td>
	<td id='td2'><a style='text-decoration:none' href='/predict/'''+country+''''>Predict</td>
	</tr></table>

	<div style="position:absolute; left:15px; top:40px">
	<table id="table3">
	<tr style="color:#eba134;text-align:center"><td></td><td>Country</td><td>Total</td><td>Last day</td></tr>
	<tr bgcolor=#eba134><td width="16px"></td><td align="left" width="150px">World</td>
	<td align="right" width="70px">'''+str(world_overall[0])+'''</td><td align="right" width="60px">'''+str(world_overall[1])+'''</td></tr>
	</table>
	</div>'''
    


    tab1 = '''<div id="div1"><table id='table1'>'''+country_overall+'''</table></div>'''

    tab2 = '''<div style = "position:absolute; left:600px; top:20px">
	<table id="table2">
	<tr><td align="center" style="color:#eba134" bgcolor="">'''+country_short[0]+'''</td>
	<td align="center" style="color:black" bgcolor="#eba134">'''+country_short[1]+'''</td></tr>
	<tr><td>Population (In Million)</td><td align="right">'''+str(country_short[2])+'''</td></tr>
	<tr><td>Population Density (sq Km)</td><td align="right">'''+str(country_short[3])+'''</td></tr>
	<tr><td>Median Age</td><td align="right">'''+str(country_short[4])+'''</td></tr>
	<tr><td>Affected People</td><td align="right">'''+str(country_short[5])+'''</td></tr>
	<tr><td>Spread Level</td><td align="center" bgcolor="'''+str(country_short[6])+'''">
	<font color="black">'''+str(country_short[7])+'''</font></td></tr>
	</table></div>'''

    chart1 = '''<div style = "position:absolute; left:360px; top:160px">
    <table>
    <tr>
    <td>
    <div id="chart1">
    <script>
    var chart = c3.generate({
    data: {
        x: 'x',
        columns: [
            ['x', ''' + dates + '''],
            ['country', ''' + plot_data[1] + '''],
            ['world', ''' + plot_data[4] + ''']
        ],
        colors: {
            world: 'red',
            country: '#eba134'
        },
        axes: {
            country: 'y',
            country: 'y2'
        }
    },
    axis: {
        x: {
            type: 'timeseries',
            tick: {
                format: '%m-%d'
            }
        },
        y2: {
            show: true
        }
    },
    bindto: '#chart1'
    });
    </script></div>

    </td>
    <td colspan="2">'''
    
    chart5 = '''
	<div id="chart5">
	<script>
	var chart = c3.generate({
	    data: {
	        x: 'x',
	        columns: [
	            ['x', ''' + dates + '''],
	            ['country', ''' + plot_data[3] + '''],
	            ['world', ''' + plot_data[6] + ''']
	        ],
	        colors: {
	            world: 'red',
	            country: '#eba134'
	        }
	    },
	    axis: {
	        x: {
	            type: 'timeseries',
	            tick: {
	                format: '%m-%d'
	            }
	        }
	    },
	    bindto: '#chart5'
	});
	</script>
	</div>
	</td>
	</tr>
	<tr>
	<td>'''
    
    chart2 = '''
	<div id="chart2">
	<script>
	var chart = c3.generate({
	    data: {
	        x: 'x',
	        columns: [
	            ['x', ''' + dates + '''],
	            ['country', ''' + plot_data[2] + '''],
	            ['world', ''' + plot_data[5] + ''']
	        ],
	        colors: {
	            world: 'red',
	            country: '#eba134'
	        },
	        axes: {
	            country: 'y',
	            world: 'y2'
	        },
	        type: 'bar'
	    },
	    axis: {
	        x: {
	            type: 'timeseries',
	            tick: {
	                format: '%m-%d'
	            }
	        },
	        y2: {
	            show: true
	        }
	    },
	    bindto: '#chart2'
	});
	</script>
	</div>
	</td>
	<td>'''
    
    chart3 = '''
	<div id="chart3">
	<script>
	var chart = c3.generate({
	    data: {
	        columns: [
	            ['Last day', '''+str(country_short[8])+'''],
	            ['Previous Week', '''+str(country_short[9])+'''],
	        ],
	        colors: {
	            'Last day': '#eba134',
	            'Previous Week': 'red'
	        },
	        type : 'pie'
	    },
	    bindto: '#chart3'
	});
	</script>
	</div>'''
    
    chart4 = '''
	</td>
	<td>
	<div id="chart4">
	<script>
	var chart = c3.generate({
	    data: {
	        columns: [
	            ['Last day', '''+str(country_short[10])+'''],
	            ['Previous Week', '''+str(country_short[11])+'''],
	        ],
	        colors: {
	            'Last day': '#eba134',
	            'Previous Week': 'red'
	        },
	        type : 'pie'
	    },
	    bindto: '#chart4'
	});
	</script>
	</div>'''

    msg_2 = '''</td></tr></table></div></body></html>'''
    
    msg = msg_1 + style + tab3 + tab1 + tab2 + chart1 + chart5 + chart2 + chart3 + chart4 + msg_2
    return msg

@route('/predict/<country>')
def main(country):
    world_overall = get_world_overall_pred()
    country_overall = get_country_overall_pred()
    plot_data = get_plots_data_pred(country)
    dates = plot_data[0]
    tab3_var = plot_data[7]
    
    msg_1 = '''
	<!DOCTYPE html>
	<html lang="en">
	<head>
	    <title></title>
	    <meta charset="utf-8" />
	    <link href="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.10/c3.min.css" rel="stylesheet" />
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.6/d3.min.js"></script>
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.10/c3.min.js"></script>
	</head>'''
    
    style = '''
    <style>
    a {text-decoration:none; font-color:white; color:white;}
	#chart1 {height: 200px;width: 700px;}
	#chart2 {height: 200px;width: 700px;}
	#chart5 {height: 200px;width: 700px;}
	table {border-collapse: collapse; color:#eba134; font-family: "Segoe UI Light"}
	#table1 {color:white;text-align:right}
	#table2 {color:white;}
	#table3 {color:black;}
	td, tr {border: 1px solid white;padding:1px 3px 1px 3px;}
	path.domain { stroke: white; }
	.tick text { fill: white; }
	.c3-legend-item text { fill: white; }
	#td1 {text-align:left; width:150px}
	#td1:hover {background-color:#eba134; font-color:black; text-color:black; color:black;}
	#td2:hover {background-color:#eba134; font-color:black; text-color:black; color:black;}
	#div1::-webkit-scrollbar {display: none;}
	#div1 {-ms-overflow-style: none;}
	#div1 {position:absolute; left:15px; top:82px; height:510px; overflow:auto;}
	</style>
    '''
    
    tab3 = '''<body bgcolor="black">

	<table style='margin-left:9px'>
	<tr style="color:white;text-align:center">
	<td id='td2'><a style='text-decoration:none' href='/overview'>Overview</td>
	<td id='td2'><a style='text-decoration:none' href='/current/'''+country+''''>Current</td>
	<td bgcolor=#eba134><font color="black">Predict</td>

	</tr></table>

	<div style = "position:absolute; left:15px; top:40px">
	<table id="table3">
	<tr style="color:#eba134;text-align:center"><td></td>
	<td>Country</td><td>Total</td><td>Estimated(+14)</td></tr>
	<tr bgcolor=#eba134><td width="16px"></td><td align="left" width="150px">World</td>
	<td align="right" width="70px">'''+str(world_overall[0])+'''</td>
	<td align="right" width="70px">'''+str(world_overall[1])+'''</td></tr>
	</table>
	</div>'''

    tab1 = '''<div id="div1"><table id='table1'>'''+country_overall+'''</table></div>'''
    
    tab2 = '''
    <div style = "position:absolute; left:600px; top:20px">
	<table id="table2">
	<tr>
	<td><object type="image/svg+xml" data="../'''+tab3_var[0]+'''.svg" height="16px" width="16px" align="center"></td>
	<td colspan="2" align="center"  style="color:#eba134">'''+country+'''</td>
	<td>Current : '''+str(tab3_var[1])+'''</td>
	<td>Estimated(14 days later) : '''+str(tab3_var[2])+'''</td></tr>
	</table>
	</div>
    '''

    chart1 = '''
    <div style = "position:absolute; left:500px; top:50px">
	<table>
	<tr>
	<td>
	<div id="chart1">
	<script>
	var chart = c3.generate({
	    data: {
	        x: 'x',
	        columns: [
	            ['x', ''' + dates + '''],
	            ['country', ''' + plot_data[1] + '''],
	            ['world', ''' + plot_data[4] + ''']
	        ],
	        colors: {
	            world: 'red',
	            country: '#eba134'
	        },
	        axes: {
	            country: 'y',
	            country: 'y2'
	        }
	    },
	    axis: {
	        x: {
	            type: 'timeseries',
	            tick: {
	                format: '%m-%d'
	            }
	        },
	        y2: {
	            show: true
	        }
	    },
	    bindto: '#chart1'
	});
	</script>
	</div>
	</td>
	</tr>
    '''

    chart2 = '''<tr>
    <td>
        <div id="chart2">
	<script>
	var chart = c3.generate({
	    data: {
	        x: 'x',
	        columns: [
	            ['x', ''' + dates + '''],
	            ['country', ''' + plot_data[2] + '''],
	            ['world', ''' + plot_data[5] + ''']
	        ],
	        colors: {
	            world: 'red',
	            country: '#eba134'
	        },
	        axes: {
	            country: 'y',
	            world: 'y2'
	        },
	        type: 'bar'
	    },
	    axis: {
	        x: {
	            type: 'timeseries',
	            tick: {
	                format: '%m-%d'
	            }
	        },
	        y2: {
	            show: true
	        }
	    },
	    bindto: '#chart2'
	});
	</script>
	    </td>
	</tr>'''
    
    chart5 = '''<tr>
	    <td>
	        <div id="chart5">
	<script>
	var chart = c3.generate({
	    data: {
	        x: 'x',
	        columns: [
	            ['x', ''' + dates + '''],
	            ['country', ''' + plot_data[3] + '''],
	            ['world', ''' + plot_data[6] + ''']
	        ],
	        colors: {
	            world: 'red',
	            country: '#eba134'
	        }
	    },
	    axis: {
	        x: {
	            type: 'timeseries',
	            tick: {
	                format: '%m-%d'
	            }
	        }
	    },
	    bindto: '#chart5'
	});
	</script>
	</td>
    '''
    msg_2 = '''</tr></table></div></body></html>'''
    
    msg = msg_1 + style + tab3 + tab1 + tab2 + chart1 + chart2 + chart5 + msg_2
    return msg

@route('/overview')
def main(country='India'):
    world_overall = get_world_overall_curr()
    country_overall, total_countries, top_country, top_aff = get_country_overall_over()
    country_short = get_country_short_over()
    plot_data = get_plots_data_over()
    dates = plot_data[0]
    latest_date = dates.split(',')[-1][1:-1]

    msg_1 = '''<!DOCTYPE html>
    <html lang="en">
    <head>
    <title></title>
    <meta charset="utf-8" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.10/c3.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.6/d3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.10/c3.min.js"></script>
    </head>
    '''
    style = '''<style>
    a {text-decoration:none; font-color:white; color:white;}
    #chart1 {height: 200px;width: 500px;}
    #chart2 {height: 200px;width: 500px;}
    #chart3 {height: 200px;width: 200px; margin-left:100px;}
    #chart4 {height: 200px;width: 200px;}
    #chart5 {height: 200px;width: 400px;}
    table {border-collapse: collapse; color:#eba134; font-family: "Segoe UI Light"}
    #table1 {color:white;text-align:right}
	#table2 {color:white;}
	#table3 {color:black;overflow:scroll;}
	#td1 {text-align:left; width:150px}
	#td1:hover {background-color:#eba134; font-color:black; text-color:black; color:black;}
	#td2:hover {background-color:#eba134; font-color:black; text-color:black; color:black;}
	td, tr {border: 1px solid white;padding:1px 3px 1px 3px;}
	path.domain { stroke: white; }
	.tick text { fill: white; }
	.c3-legend-item text { fill: white; }
	#div1::-webkit-scrollbar {display: none;}
	#div1 {-ms-overflow-style: none;}
	#div1 {position:absolute; left:700px; top:100px}
	</style>'''
   
    tab3 = '''<body bgcolor="black">

	<table style='margin-left:9px'>
	<tr style="color:white;text-align:center">
	<td bgcolor=#eba134><font color="black">Overview</td>
	<td id='td2'><a style='text-decoration:none' href='/current/'''+country+''''>Current</td>
	<td id='td2'><a style='text-decoration:none' href='/predict/'''+country+''''>Predict</td>
	</tr></table>
    
	<div style="position:absolute; left:110px; top:40px; color:white; width:500px">
	<p>The 2019–20 coronavirus pandemic is an ongoing pandemic of coronavirus disease 2019 (COVID-19),
	caused by severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2).
	The outbreak was first identified in Wuhan, Hubei Province, China, in December 2019.
	The World Health Organization (WHO) declared the outbreak to be a Public Health Emergency of International Concern on 30 January 2020
	and recognized it as a pandemic on 11 March 2020. As of latest data(''' + latest_date + '''), more than ''' + str(world_overall[0]) + ''' cases of
	COVID-19 have been reported in around ''' + str(total_countries) + ''' countries and territories. ''' + top_country + ''' is
	the most affected country with ''' + str(top_aff) + ''' affected people
	</p>
	</div>'''
    
    tab1 = '''    
	<div style="position:absolute; left:700px; top:58px">
    <table id="table3">
	<tr style="color:#eba134;text-align:center"><td></td><td>Country</td><td>Total</td><td>Last day</td></tr>
	<tr bgcolor=#eba134><td width="16px"></td><td align="left" width="150px">World</td>
	<td align="right" width="70px">'''+str(world_overall[0])+'''</td><td align="right" width="60px">'''+str(world_overall[1])+'''</td></tr>
	</table>
    </div>
    <div id="div1"><table id='table1'>'''+country_overall+'''</table></div>'''

    chart1 = '''<div style = "position:absolute; left:110px; top:230px">
    <table>
    <tr>
    <td>
    <div id="chart1">
    <script>
    var chart = c3.generate({
    data: {
        x: 'x',
        columns: [
            ['x', ''' + dates + '''],
            ['world', ''' + plot_data[1] + ''']
        ],
        colors: {
            world: 'red',
        }
    },
    axis: {
        x: {
            type: 'timeseries',
            tick: {
                format: '%m-%d'
            }
        }
    },
    bindto: '#chart1'
    });
    </script></div>

    </td>
    '''
    
    chart5 = '''
    <td>
	<div id="chart5">
	<script>
	var chart = c3.generate({
	    data: {
	        x: 'x',
	        columns: [
	            ['x', ''' + dates + '''],
	            ['world', ''' + plot_data[3] + '''],
	        ],
	        colors: {
	            world: 'red',
	        }
	    },
	    axis: {
	        x: {
	            type: 'timeseries',
	            tick: {
	                format: '%m-%d'
	            }
	        }
	    },
	    bindto: '#chart5'
	});
	</script>
	</div>
	</td>
	</tr>
	<tr>
	<td>'''
    
    chart2 = '''
	<div id="chart2">
	<script>
	var chart = c3.generate({
	    data: {
	        x: 'x',
	        columns: [
	            ['x', ''' + dates + '''],
	            ['world', ''' + plot_data[2] + '''],
	        ],
	        colors: {
	            world: 'red',
	        },
	        type: 'bar'
	    },
	    axis: {
	        x: {
	            type: 'timeseries',
	            tick: {
	                format: '%m-%d'
	            }
	        }
	    },
	    bindto: '#chart2'
	});
	</script>
	</div>
	</td>
	<td>'''
    
    chart3 = '''
	<div id="chart3">
	<script>
	var chart = c3.generate({
	    data: {
	        columns: [
	            ['Last day', '''+str(country_short[0])+'''],
	            ['Previous Week', '''+str(country_short[1])+'''],
	        ],
	        colors: {
	            'Last day': '#eba134',
	            'Previous Week': 'red'
	        },
	        type : 'pie'
	    },
	    bindto: '#chart3'
	});
	</script>
	</div>'''

    msg_2 = '''</td></tr></table></div></body></html>'''
    
    msg = msg_1 + style + tab3 + tab1 + chart1 + chart5 + chart2 + chart3 + msg_2
    return msg

run(host=config.HOST, port=config.PORT)
