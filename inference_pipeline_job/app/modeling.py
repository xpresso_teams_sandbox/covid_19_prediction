''' insert comments '''
import warnings
import pandas as pd
import numpy as np
from fbprophet import Prophet
from sklearn.linear_model import LinearRegression
import helper
import config
warnings.filterwarnings("ignore")

def prophet_train(data, country_name):
    ''' insert comments '''
    data_filt = data.rename({'Date': 'ds', 'Daily_log': 'y'}, axis=1)
    model = Prophet(weekly_seasonality=False)
    model.fit(data_filt)
    filename = config.MASTER_PATH + 'data/model/prophet/' + country_name + '.pkl'
    helper.save_model(model, filename)

def lin_reg_train(data, country_name):
    ''' insert comments '''
    model = LinearRegression()
    records = len(data)
    start_date = list(data.tail(1)['Date'])[0]
    data['xval'] = range(1, records + 1)
    xfit = data['xval'].values.reshape(-1, 1)
    yfit = data['Daily_log'].values.reshape(-1, 1)
    model.fit(xfit, yfit)
    intercept = float(model.intercept_[0])
    slope = float(model.coef_[0])
    model_param = {'records': records, 'start_date': start_date, 'intercept': intercept, 'slope': slope}
    filename = config.MASTER_PATH + 'data/model/lin_regr/' + country_name + '.pkl'
    helper.save_model(model_param, filename)

def generating_model(countries):
    ''' insert comments '''
    for country in countries:
        country_name = country.replace(' ', '_')
        country_train_data = helper.feature_transform(country_name)
        prophet_train(country_train_data, country_name)
        lin_reg_train(country_train_data, country_name)

def get_global_model(days=15):
    ''' insert comments '''
    world_data = pd.read_csv(config.MASTER_PATH + 'data/intermediate/world_data.csv')
    world_data = world_data.tail(days)
    world_data['Daily_log'] = world_data['Daily'].map(np.log)
    records = len(world_data)
    model = LinearRegression()
    world_data['xval'] = range(1, records + 1)
    xfit = world_data['xval'].values.reshape(-1, 1)
    yfit = world_data['Daily_log'].values.reshape(-1, 1)
    model.fit(xfit, yfit)
    slope = float(model.coef_[0])
    return slope
