''' insert comments '''
import warnings
import pandas as pd
import helper
import predict
import ui_data
import config
warnings.filterwarnings("ignore")

def main():
    #Fetching test data
    TEST_COUNTRIES = helper.load_test_data()

    COUNTRY_LATEST_DATA = pd.read_csv(config.MASTER_PATH + 'data/ui/current/country_latest_data.csv')
    COUNTRY_VALID = list(COUNTRY_LATEST_DATA[COUNTRY_LATEST_DATA['Confirmed'] >= 100]['Country'])
    COUNTRY_INVALID = list(COUNTRY_LATEST_DATA[COUNTRY_LATEST_DATA['Confirmed'] < 100]['Country'])
    COUNTRY_VALID = [item for item in COUNTRY_VALID if item in TEST_COUNTRIES]
    COUNTRY_INVALID = [item for item in COUNTRY_INVALID if item in TEST_COUNTRIES]

    #Loading model and predicting on test data
    predict.predicting_model(COUNTRY_VALID)
    predict.predicting_invalid(COUNTRY_INVALID)

    #Exporting predictions
    helper.aggregate_pred(config.MASTER_PATH + 'data/output/prophet')

    #Create data for UI
    ui_data.prepare_pred_data()
